import configparser
import pymysql

def init():
    # 读取配置文件
    cf = configparser.ConfigParser()
    cf.read("../spider.conf", "utf-8-sig")
    mysql_host = cf.get("mysql", "mysql_host")
    mysql_port = cf.get("mysql", "mysql_port")
    mysql_user = cf.get("mysql", "mysql_user")
    mysql_pwd = cf.get("mysql", "mysql_pwd")
    mysql_schema = cf.get("mysql", "mysql_schema")
    return pymysql.connect(mysql_host, mysql_user, mysql_pwd, mysql_schema)


def insert(sql):
    db = init()
    # 使用cursor()方法获取操作游标
    cursor = db.cursor()

    try:
        # 执行sql语句
        cursor.execute(sql)
        # 提交到数据库执行
        db.commit()
        # print("成功插入一条记录")
    except:
        # 如果发生错误则回滚
        db.rollback()
        print("插入数据库异常")

    # 关闭数据库连接
    db.close()


if __name__ == '__main__':
    insert("insert into c_author(author_name,description,dynasty) values('你好','打发顺丰','先秦');")