import requests
import re
import arrow
from lxml import etree
from tools import mysql_util

dynasty = "清代"
pageNo = 440
pre_sql = "insert into c_author(author_name,description,dynasty) values('%s','%s','%s');"

#处理爬到的网页
def handle_page(page):
    page = etree.HTML(page)

    records = page.xpath("//div[@class='sonspic']")
    for record in records:
        author = record.xpath(".//a/b")
        description = record.xpath(".//p")
        sql = pre_sql % (author[0].text,description[1].text,dynasty)
        # print(sql)
        mysql_util.insert(sql)


#通过头信息伪装成火狐浏览器
headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Language': 'ezh-CN,zh;q=0.9',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36'
}



init_url = "https://so.gushiwen.org/authors/Default.aspx?p=%d&c=%s"
filePath = 'C:/d/spider/standalone/'
count = 0

start_time = arrow.now().timestamp

for i in range(pageNo):

    url = init_url % (i+1,dynasty)
    r = requests.get(url, headers=headers)

    # print (re.findall('<b>(.*?)</b>', r.text), re.S)
    print("处理第%d页",i+1)
    handle_page(r.text)
    # print(r.text)



    # print('已经抓取：' + str(count) + '个，正在抓取-->' + url)
    # count += 1
    # fname = tool.url_replace(url)
    #
    # try:
    #     req = urllib.request.Request(url,headers = headinfo)
    #     urlop = urllib.request.urlopen(req,timeout = 2)
    #     data = urlop.read().decode('utf-8')
    #     saveToFile(filePath + fname + '.html', data)
    # except:
    #     continue
    #
    # linkre = re.compile('href="(.+?)"')
    # linkdata = linkre.findall(data)

end_time = arrow.now().timestamp
# print("爬取" + str(count) + "个网页，花费" + str(end_time - start_time) + "秒")